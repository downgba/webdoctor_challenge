require 'csv_parser'

class Finder

  def initialize(file)
    @agenda = CSVParser.parse(file)
  rescue CSVParser::InvalidFile => e
    puts e.message # here, we could log the problem or treat it in a different way
  end

  # Displays doctors' availability
  # @param day [String] day to check the availabilty e.g.: Monday, Saturday
  # @return nil
  def find_doctor(day)
    available_doctors = @agenda[day.downcase.to_sym] || Array.new

    if available_doctors.empty?
      puts 'There is no doctor available'
    else
      available_doctors.each { |name| puts "#{name} is available" }
    end
  end
end
