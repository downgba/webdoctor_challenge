require 'csv'

class CSVParser
  class InvalidFile < StandardError; end;

  AVAILABLE = 'ON'.freeze

  # Extract doctors availability from the informed file
  # @param file [String] path for the file
  # @return Hash[<String><Array>] hash with doctors availability <day, doctors>
  def self.parse(file)
    result  = Hash.new
    headers = []

    CSV.foreach(file) do |line|
      if result.empty?
        headers = line
        for i in 1..(line.length - 1)
          result[line[i].downcase.to_sym] = Array.new
        end
      else
        doctor_name = line[0]

        for i in 1..(line.length - 1)
          result[headers[i].downcase.to_sym] << doctor_name  if AVAILABLE == line[i]
        end
      end
    end

    raise InvalidFile.new('Empty File') if result.empty?

    result
  end
end
