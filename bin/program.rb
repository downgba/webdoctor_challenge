#!/usr/bin/env ruby

$LOAD_PATH.unshift(File.expand_path('../../lib', __FILE__))
require 'finder'

Finder.new(ARGV[0]).find_doctor(ARGV[1])
