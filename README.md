# WebDoctor Challenge

I split the code in two diffents classes, CSVParser and Finder.

```
    - CSVParser is responsible for reading the file and create a hash with doctors' availability
        Hash<week_day[String], doctors_name[Array]>
    - Finder just calls CSVParser and goes through the Array of names, displaying them.
```

I used the CSV.foreach instead of CSV.read, because CSV.read loads the whole file into the memory, what could be a problem depending on 
the size of the file

### Performing the tests

```
    bundle
    rspec
```

### Running the code

```
    ruby bin/program.rb doctors.csv 'tuesday'
```