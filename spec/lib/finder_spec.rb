require 'spec_helper'

describe Finder do
  subject(:finder) { described_class.new(file) }

  let(:file) { 'spec/fixtures/doctors.csv' }

  describe '#initialize' do
    let(:file) { 'spec/fixtures/empty_file.csv' }

    it 'treats InvalidFile exception' do
      expect{finder}.to output("Empty File\n").to_stdout
    end
  end

  describe '#find_doctor' do
    it 'displays doctors availability regardless capital letters' do
      expect{finder.find_doctor('Thursday')}.to output("Dr. Adamski is available\n").to_stdout
      expect{finder.find_doctor('Thursday')}.to output("Dr. Adamski is available\n").to_stdout
    end

    it 'displays all available doctors' do
      expect{finder.find_doctor('tuesday')}.to output("Dr. Adamski is available\nDr. May is available\n").to_stdout
    end

    it 'displays "There is no doctor available"' do
      expect{finder.find_doctor('friday')}.to output("There is no doctor available\n").to_stdout
    end

    context 'when an invalid day is informed' do
      it 'displays "There is no doctor available"' do
        expect{finder.find_doctor('foo')}.to output("There is no doctor available\n").to_stdout
      end
    end
  end
end
