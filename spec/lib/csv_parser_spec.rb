require 'spec_helper'

describe CSVParser do
  describe '.parser' do
    context 'valid file' do
      let(:file) { 'spec/fixtures/doctors.csv' }
      let(:result) do
        {
          monday:    ['Dr. Kim', 'Dr. May'],
          tuesday:   ['Dr. Adamski', 'Dr. May'],
          wednesday: ['Dr. Kim', 'Dr. Adamski'],
          thursday:  ['Dr. Adamski'],
          friday:    [],
          saturday:  ['Dr. Kim', 'Dr. Adamski', 'Dr. May'],
          sunday:    ['Dr. Kim', 'Dr. May']
        }
      end

      it 'returns a hash with available doctors' do
        expect(CSVParser.parse(file)).to eq(result)
      end
    end

    context 'empty file' do
      let(:file) { 'spec/fixtures/empty_file.csv' }

      it 'raises InvalidFile expection' do
        expect{CSVParser.parse(file)}.to raise_error(CSVParser::InvalidFile, 'Empty File')
      end
    end
  end
end
